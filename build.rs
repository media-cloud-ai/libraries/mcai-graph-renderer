use enum_iterator::IntoEnumIterator;
use material_icons::{icon_to_html_name, Icon};
use reqwest::StatusCode;
use std::io::Write;
use std::path::Path;
use svg::{node::element::tag, parser::Event};

fn main() {
  let icons: Vec<String> =
    Icon::into_enum_iter().filter_map(|i|{
      let html_name = icon_to_html_name(&i);
      let filepath = format!("icon/{}.svg", html_name);
      let path = Path::new(&filepath);

      let svg = if path.exists() {
        Some((html_name, std::fs::read(path).unwrap()))
      } else {
        let res = reqwest::blocking::get(format!("https://fonts.gstatic.com/s/i/short-term/release/materialsymbolsoutlined/{}/default/48px.svg", html_name)).unwrap();
        match res.status() {
          StatusCode::OK => {
            let svg = res.bytes().unwrap();
            std::fs::write(format!("icon/{}.svg", html_name), svg.clone()).unwrap();
            Some((html_name, svg.to_vec()))
          }
          StatusCode::NOT_FOUND => {
            return None;
          }
          _ => {
            panic!("Couldn't download icon")
          }
        }
      };

      svg.map(|(name, svg)|{
        let mut parser = svg::read(std::str::from_utf8(&svg).unwrap()).unwrap();
        let element = parser.find(|elem| matches!(elem, Event::Tag(tag::Path, _, _))).unwrap();
        let attr = if let Event::Tag(_, _, attributes) = element {Some(attributes)} else {None};
        let attributes = attr.unwrap();

        (name.to_string(), attributes.get("d").unwrap().to_string())
      })
  }).map(|(name, d)|{
    format!("(\"{}\", \"{}\")", name, d)
  }).collect();

  let dst = std::path::Path::new(&std::env::var("OUT_DIR").unwrap()).join("icons.rs");
  let mut icon_file = std::fs::File::create(&dst).unwrap();
  let content = format!("HashMap::from([{}])", icons.join(","));
  icon_file.write_all(content.as_bytes()).unwrap();
}
