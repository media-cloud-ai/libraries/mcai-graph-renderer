use std::fmt::{self, Display, Formatter};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
  FetchIcon(reqwest::Error),
  ParseWorkflow(String),
}

impl From<std::io::Error> for Error {
  fn from(error: std::io::Error) -> Self {
    Error::ParseWorkflow(error.to_string())
  }
}

impl From<serde_json::Error> for Error {
  fn from(error: serde_json::Error) -> Self {
    Error::ParseWorkflow(error.to_string())
  }
}

impl From<reqwest::Error> for Error {
  fn from(error: reqwest::Error) -> Self {
    Error::FetchIcon(error)
  }
}

impl Display for Error {
  fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
    match self {
      Error::FetchIcon(error) => write!(f, "Unable to fetch icon: {:?}", error),
      Error::ParseWorkflow(error) => write!(f, "Could not parse workflow: {:?}", error),
    }
  }
}
